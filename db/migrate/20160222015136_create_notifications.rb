class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.text :contenu
      t.string :link
      t.string :notification_id
      t.string :user_id

      t.timestamps null: false
    end
  end
end
