Rails.application.routes.draw do

  root                'static_pages#home'
  get    'help'    => 'static_pages#help'
  get    'about'   => 'static_pages#about'
  get    'contact' => 'static_pages#contact'
  get    'signup'  => 'users#new'
  get    'login'   => 'sessions#new'
  get    'notifications/show_all', to: 'notifications#show_all', as: 'notifications'
  post   'login'   => 'sessions#create'
  delete 'logout'  => 'sessions#destroy'
<<<<<<< HEAD
  get 	'test'	=>	'notifications#test'
=======
>>>>>>> 8764b3c22486d84ce9368cb5a5266bcb6bf665e6
  resources :users do
    member do
      get :following, :followers
    end
    resources :conversations do
      resources :messages
    end
  end
  resources :conversations do
    resources :messages
  end

  get     '/messages/new', :to => 'messages#new', :as => :messages_path

  resources :account_activations, only: [:edit]
  resources :password_resets,     only: [:new, :create, :edit, :update]
  resources :microposts,          only: [:create, :destroy]
  resources :relationships,       only: [:create, :destroy]
  resources :notifications,       only: [:create, :show, :destroy]  #Création, affichage et suppression des notifications
end
