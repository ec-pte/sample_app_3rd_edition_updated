class Notification < ActiveRecord::Base

  #c'est la classe qui représente les notifications et les relations qu'elles
  #entretiennent avec les classes user
  belongs_to :user

  validates :user_id, presence: true
  validates :contenu, presence: true, length: { maximum: 140 }
  #validates :seen?, presence:true

end