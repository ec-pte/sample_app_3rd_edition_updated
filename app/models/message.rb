class Message < ActiveRecord::Base

  #c'est la classe qui représente les messages  et les relation qu'elles
  #entretiennent avec les classes user et conversation
  belongs_to :conversation
  belongs_to :user

  validates_presence_of :body, :conversation_id, :user_id

end