class ApplicationMailer < ActionMailer::Base
  default from: "example@railstutorial.org"
  layout 'mailer'
end
