/**
 * Created by papiindiiay on 16-02-08.
 */

/**
 * ready est une fonction qui écoute les différentes actions faites par un utilisateur
 * et appel en fonction de ce qui est fait la focntion appoprié a l'action
 * Ex : si l'utilisateur appuie sur fermer le chat, ready va faire appel a la fonction closechat
 * qui va fermer la discussion instantannée que nous avons selectionné
 */
var ready = function () {


    $('.start-conversation').click(function (e){

        e.preventDefault();

        var sender_id = $(this).data('sid');
        var recipient_id = $(this).data('rip') ;

        $.post("/conversations", {sender_id: sender_id, recipient_id: recipient_id}, function(data) {
         chatBox.chatWith(data.conversation_id);
        });
    });


    /**
     * fonction utilisé pour réduire la discussion lors d'un clique
     */

    $(document).on('click', '.toggleChatBox', function (e){
        e.preventDefault();

        var id = $(this).data('cid');
        chatBox.togglechatBoxGrowth(id) ;
    }) ;


    /**
     * fonction de fermeture de la discussion
     */

    $(document).on('click', '.closeChat', function (e) {
        e.preventDefault() ;

        var id = $(this).data('cid') ;
        chatBox.close(id);
    }) ;


    /**
     *
     * cette fonction suit les pression effectué sur le clavier au niveau de la barre
     * d'ecriture du chat  et appel la fonction chatInputkey dans chat.js
     * pour la vérification
     */

     $(document).on('keydown', 'chatboxtextarea', function (event){

      var id = $(this).data('cid');
      chatBox.checkInputKey(event, $(this), id) ;

     });

    /**
     *
     * fonction qui lorsqu'on appuie sur la barre
     * de discussion fait apparaître le chat
     */

    $('a.conversation').click(function (e){

        e.preventDefault();

        var conversation_id = $(this).data('cid');
        chatBox.chatWith(conversation_id);
    }) ;



}

$(document).ready(ready);
$(document).on("page:load",ready);
