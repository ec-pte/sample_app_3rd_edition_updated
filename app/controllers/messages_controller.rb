class MessagesController < ApplicationController
  #before_filter :authenticate_user!

  def create
    @conversation = Conversation.find(params[:conversation_id])
    @message = @conversation.messages.build(message_params)
    @message.user_id = current_user.id
    @message.save

    ### Ici, on enregistre la notification pour l'autre ##################
    n = Notification.new                                                 #
    n.user_id = @conversation.recipient_id                               #
    if(@conversation.messages.first == @message)                         #
      n.contenu = current_user.name + " started a conversation with you" #
    else                                                                 #
      n.contenu = "You've got a message from " + current_user.name       #
    end                                                                  #
    n.save                                                               #
    ######################################################################

    @path = conversation_path(@conversation)
  end

  private

  def message_params
    params.require(:message).permit(:body)
  end
end