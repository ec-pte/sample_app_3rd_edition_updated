class RelationshipsController < ApplicationController
  before_action :logged_in_user

  def create
    @user = User.find(params[:followed_id])
    current_user.follow(@user)

    ### Ici, on envoie une notification au follower #############
    flash[:success] = "You started following " + @user[:name]   #
    #############################################################

    ### Ici, on enregistre la notification pour l'autre #########
    n = Notification.new                                        #
    n.user_id = current_user.id                                 #
    n.contenu = current_user.name + " started following you"    #
    n.save                                                      #
    #############################################################

    respond_to do |format|
      format.html { redirect_to @user }
      ### On actualise ensuite la page ####################
      format.js {render inline: "location.reload();" }    #
      #####################################################
    end
  end
  
  def destroy
    @user = Relationship.find(params[:id]).followed
    current_user.unfollow(@user)

    ### Ici, on envoie une notification au unfollower #########
    flash[:success] = "You just unfollowed " + @user[:name]   #
    ###########################################################
    respond_to do |format|
      format.html { redirect_to @user }
      ### On actualise ensuite la page ####################
      format.js {render inline: "location.reload();" }    #
      #####################################################
    end
  end
end
